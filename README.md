# cloud-bigdata

Talk slides from MRC Centre Gregynog retreat 2018 - Why aren't we using the cloud?

The source is available in the gh-pages branch of this repository.

The slides can be viewed at:

http://einon.net/cloud-bigdata
